"use strict";

const button = document.getElementById("button");
button.style.display = "block";

const input = document.createElement("input");
input.id = "input";

const buttonPaint = document.createElement("button");
buttonPaint.id = "buttonPaint";
buttonPaint.textContent = "Нарисовать";

button.addEventListener("click", function () {
  if (
    document.getElementById("input") !== null ||
    document.getElementById(buttonPaint) !== null
  ) {
    input.remove();
    buttonPaint.remove();
  }
  button.after(input);
  input.after(buttonPaint);
});

buttonPaint.addEventListener("click", function () {
  // Delete existed
  if (document.getElementById("wrapper") !== null) {
    document.getElementById("wrapper").remove();
  }

  const wrapper = document.createElement("div");
  wrapper.id = "wrapper";
  wrapper.style.width = `${input.value * 10}px`;
  wrapper.style.display = "flex";
  wrapper.style.flexWrap = "wrap";

  for (let i = 0; i < 100; i++) {
    const circle = document.createElement("div");
    circle.className = "circle";
    circle.style = `height: ${input.value}px; width: ${
      input.value
    }px; background-color: rgb(${randomCol()}); border-radius: 50%; display: inline-block; cursor: pointer;`;

    wrapper.append(circle);
  }

  wrapper.addEventListener("click", function (e) {
    e.target.remove();
  });

  buttonPaint.after(wrapper);
});

function randomCol() {
  const firstCol = parseInt(Math.random() * 255);
  const secondCol = parseInt(Math.random() * 255);
  const thirdCol = parseInt(Math.random() * 255);

  return `${firstCol}, ${secondCol}, ${thirdCol}`;
}
